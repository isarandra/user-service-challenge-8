package com.codewithisa.userservice.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@Slf4j
public class FirstServiceScheduler {

//    @Scheduled(
//            cron = "* * * * * *"
//    )
    public void firstMethod(){
        log.info("First job test using Scheduler " + LocalDateTime.now());
    }
}
